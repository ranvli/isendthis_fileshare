﻿using CompArchivos.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CompArchivos.Controllers
{
    public class FileController : Controller
    {

        DownloadFiles obj;
        public FileController()
        {
            obj = new DownloadFiles();
        }


        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, string max_descarga)
        {
            try
            {
                Guid fileGuid = Guid.NewGuid();
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    dbArchivosManager.AddArchivo(fileName, fileGuid.ToString(), max_descarga);
                    var path = Path.Combine(Server.MapPath("~/archivos/"), fileGuid.ToString());
                    file.SaveAs(path);
                }
                //ViewBag.Message = "Archivo cargado, para compartir el archivo usa el siguiente link: " + HttpContext.Request.Url.Host + "/DownloadGuid?fileGuid=" + fileGuid.ToString();
                TempData["Message"] = "Archivo cargado, para compartir el archivo usa el siguiente link: " + HttpContext.Request.Url.Host + "/File/DownloadGuid?fileGuid=" + fileGuid.ToString();
                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.Message = "Carga fallida";
                return RedirectToAction("Upload");
            }
        }

        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }

        public ActionResult Index()
        {
            if (TempData["Message"] == null) TempData["Message"] = "Hola :)";
            var filesCollection = obj.GetFiles();
            return View(filesCollection);
        }

        public FileResult Download(string FileID)
        {
            int CurrentFileID = Convert.ToInt32(FileID);
            var filesCol = obj.GetFiles();
            string CurrentFileName = (from fls in filesCol
                                      where fls.FileId == CurrentFileID
                                      select fls.FilePath).First();

            string contentType = string.Empty;

            if (CurrentFileName.Contains(".pdf"))
            {
                contentType = "application/pdf";
            }
            else if (CurrentFileName.Contains(".docx"))
            {
                contentType = "application/docx";
            }
            else
            {
                contentType = "application/octet-stream";
            }

            return File(CurrentFileName, contentType, CurrentFileName);
        }


        [HttpGet]
        public ActionResult FileError()
        {
            return View("NoDisponible");
        }

        private void RemoveFile(string fileGuid)
        {
            FileInformation fileinf = dbArchivosManager.GetArchivoXguid(fileGuid.ToString());
            try
            {
                var fileName = Path.GetFileName(fileinf.FileName);
                var path = Path.Combine(Server.MapPath("~/archivos/"), fileGuid.ToString());

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    var result = dbArchivosManager.DeleteArchivo(fileGuid);
                }
            }
            catch (IOException)
            {

            }
        }

        public ActionResult DownloadGuid(string fileGuid)
        {
             
            FileInformation fileinf = dbArchivosManager.GetArchivoXguid(fileGuid.ToString());

            
            if (fileinf == null)
            {
                return RedirectToAction("FileError");
            }
            else if(fileinf.MaxDescarga <= 0)
            {
                return RedirectToAction("FileError");
            }

            else
            {
                var result = dbArchivosManager.Substract(fileGuid);
                string contentType = string.Empty;

                if (fileinf.FileName.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }
                else if (fileinf.FileName.Contains(".docx"))
                {
                    contentType = "application/docx";
                }
                else
                {
                    contentType = "application/octet-stream";
                }
                var path = Path.Combine(Server.MapPath("~/archivos/"), fileGuid.ToString());
                var bytes = System.IO.File.ReadAllBytes(path);
                if (fileinf.MaxDescarga == 1)
                    RemoveFile(fileGuid);
                return File(bytes, contentType, fileinf.FileName);
            }
        }

    }
}