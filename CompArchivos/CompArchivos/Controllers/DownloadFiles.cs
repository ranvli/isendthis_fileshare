﻿using CompArchivos.Models;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;

namespace CompArchivos.Controllers
{
    internal class DownloadFiles
    {

        public List<FileInformation> GetFiles()
        {
            List<FileInformation> lstFiles = new List<FileInformation>();
            DirectoryInfo dirInfo = new DirectoryInfo(HostingEnvironment.MapPath("~/archivos"));

            int i = 0;
            foreach (var item in dirInfo.GetFiles())
            {
                lstFiles.Add(new FileInformation()
                {
                    FileId = i + 1,
                    FileName = item.Name,
                    FilePath = dirInfo.FullName + @"\" + item.Name
                });
                i = i + 1;
            }
            return lstFiles;
        }
    }
}