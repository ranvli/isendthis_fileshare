﻿using CompArchivos.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Web;

namespace CompArchivos
{
    public class dbArchivosManager
    {

        public static List<string> GetArchivos()
        {
            string connectionString = @"Data Source=" + HttpContext.Current.Server.MapPath(@"~\dbArchivos.db") + "; Version=3;"; ;

            List<string> archivos = new List<string>();
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connectionString))
                {
                    conn.Open();
                    string sql = "SELECT * FROM archivos";


                    using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                    {
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                archivos.Add(reader["nombre_archivo"].ToString());
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (SQLiteException e)
            {

            }
            return archivos;
        }
        
        public static string AddArchivo(string nombre_archivo, string guid, string max_descarga)
        {
            string connectionString = @"Data Source=" + HttpContext.Current.Server.MapPath(@"~\dbArchivos.db") + "; Version=3;"; ;


            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "INSERT INTO archivos(guid, nombre_archivo, max_descarga) VALUES (@guid, @nombre_archivo, @max_descarga)";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@guid", guid);
                    cmd.Parameters.AddWithValue("@nombre_archivo", nombre_archivo);
                    cmd.Parameters.AddWithValue("@max_descarga", max_descarga);
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {

                    }
                }
                conn.Close();
            }
            return result != -1 ? "Arcvhiov agregado." : "Error tratando de agregar archivo";
        }
        
        public static string DeleteArchivo(string guid)
        {
            string connectionString = @"Data Source=" + HttpContext.Current.Server.MapPath(@"~\dbArchivos.db") + "; Version=3;"; ;

            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "DELETE FROM archivos WHERE guid = @guid";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@guid", guid);
                    
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {

                    }
                }
                conn.Close();
            }
            return result != -1 ? "Archivo borrado." : "Error tratando de borrar archivo";
        }

        public static void DeleteArchivoLocal(string guid)
        {

        }

        public static FileInformation GetArchivoXguid(string guid)
        {
            string connectionString = @"Data Source=" + HttpContext.Current.Server.MapPath(@"~\dbArchivos.db") + "; Version=3;"; ;

            FileInformation archivo = null;
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connectionString))
                {
                    conn.Open();
                    string sql = "SELECT * FROM archivos WHERE guid = '" + guid +"'";

                    using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                    {
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                reader.Read();
                                archivo = new FileInformation();
                                archivo.FileName = Convert.ToString(reader["nombre_archivo"].ToString());
                                archivo.MaxDescarga = Convert.ToInt32(reader["max_descarga"]);
                                archivo.Guid = new Guid(reader["guid"].ToString());
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (SQLiteException e)
            {

            }
            return archivo;
        }

        public static int Substract(string guid)
        {
            int result = -1;
            string connectionString = @"Data Source=" + HttpContext.Current.Server.MapPath(@"~\dbArchivos.db") + "; Version=3;";

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connectionString))
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "UPDATE archivos  SET max_descarga = max_descarga - 1 WHERE guid = @guid;";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@guid", guid);
                        try
                        {
                            result = cmd.ExecuteNonQuery();
                        }
                        catch (SQLiteException e)
                        {

                        }
                    }
                    conn.Close();
                }
            }
            catch (SQLiteException)
            {

            }

            return result;
        }

    }
}