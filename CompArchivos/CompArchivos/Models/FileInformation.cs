﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompArchivos.Models
{
    public class FileInformation
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int MaxDescarga { get; set; }

        public Guid Guid { get; set; }
    }
}